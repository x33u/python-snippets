## == import modules
import getpass
import bcrypt

## == type in passwd
password = getpass.getpass("password: ")

## == hash passwd
hashed_password = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt())

## == print output
print(hashed_password.decode())
